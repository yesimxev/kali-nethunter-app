/*

 Created by AnglerVonMur on 26.07.15.

*/
package com.offsec.nethunter;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.NonNull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class SQLPersistence extends SQLiteOpenHelper {

  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_NAME = "KaliLaunchers";

  private static final String CREATE_LAUNCHER_TABLE =
      String.format(
          "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT )",
          LauncherApp.TABLE, LauncherApp.ID, LauncherApp.BTN_LABEL, LauncherApp.CMD);

  public SQLPersistence(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_LAUNCHER_TABLE);
  }

  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(String.format("DROP TABLE IF EXISTS %s", SearchSploit.TABLE));
    this.onCreate(db);
  }

  public long addApp(@NonNull final String btn_name, final String command) {
    long id = 0;
    if (Objects.requireNonNull(btn_name).length() > 0 && command.length() > 0) {

      SQLiteDatabase db = this.getWritableDatabase();

      ContentValues values = new ContentValues();
      values.put(LauncherApp.BTN_LABEL, Objects.requireNonNull(btn_name));
      values.put(LauncherApp.CMD, command);

      id = db.insert(LauncherApp.TABLE, null, values);
      db.close();
    }
    return id;
  }

  public List<LauncherApp> getAllApps() {
    List<LauncherApp> apps = new LinkedList<>();
    String query = String.format("SELECT  * FROM %s", LauncherApp.TABLE);

    SQLiteDatabase db = this.getWritableDatabase();
    @SuppressLint("Recycle")
    Cursor cursor = db.rawQuery(query, null);

    LauncherApp app;
    if (cursor.moveToFirst()) {
      do {
        app = new LauncherApp();
        app.setId(Long.parseLong(cursor.getString(0)));
        app.setBtn_label(cursor.getString(1));
        app.setCommand(cursor.getString(2));
        apps.add(app);
      } while (cursor.moveToNext());
    }
    return apps;
  }

  public LauncherApp getApp(final long id) {
    LauncherApp app = null;
    if (id != 0) {
      SQLiteDatabase db = this.getReadableDatabase();

      @SuppressLint("Recycle")
      Cursor cursor =
          db.query(
              LauncherApp.TABLE,
              LauncherApp.COLUMNS,
              " id = ?",
              new String[] {String.valueOf(id)},
              null,
              null,
              null,
              null);

      if (cursor != null) cursor.moveToFirst();

      app = new LauncherApp();
      assert cursor != null;
      app.setId(Long.parseLong(cursor.getString(0)));
      app.setBtn_label(cursor.getString(1));
      app.setCommand(cursor.getString(2));
    }
    return app;
  }

  public void updateApp(final LauncherApp app) {
    if (app != null) {
      SQLiteDatabase db = this.getWritableDatabase();

      ContentValues values = new ContentValues();
      values.put(LauncherApp.BTN_LABEL, app.getBtn_label());
      values.put(LauncherApp.CMD, app.getCommand());

      db.update(
          LauncherApp.TABLE,
          values,
          LauncherApp.ID + " = ?",
          new String[] {String.valueOf(app.getId())});

      db.close();
    }
  }

  public void deleteApp(final long id) {
    if (id != 0) {
      SQLiteDatabase db = this.getWritableDatabase();

      db.delete(LauncherApp.TABLE, LauncherApp.ID + " = ?", new String[] {String.valueOf(id)});

      db.close();
    }
  }
}
